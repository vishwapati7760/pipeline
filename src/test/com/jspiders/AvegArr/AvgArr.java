package test.com.jspiders.AvegArr;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import main.java.com.jspiders.AvgArr.Avgarr;

class AvgArr {

	@Test
	void test() {
		assertEquals(10,Avgarr.avg(new  int [] {1,2,3,4,5,6,7,8,9,10}));
	}

}
